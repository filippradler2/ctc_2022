package main

import "fmt"

func fibonacci(n int) {
	f0 := 0
	f1 := 1
	tmp := 0
	if n <= 1 {
		fmt.Println(f1)
		return
	}
	for i := 0; i < n; i++ {
		fmt.Println(f1)
		tmp = f0 + f1
		f0 = f1
		f1 = tmp
	}
}

func main() {
	n := 15 // ZADEJ CISLO
	fibonacci(n)
}
